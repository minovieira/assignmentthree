import ballerina/io;
import ballerinax/kafka;

// import ballerina/http;

// Prints `Hello World`.

kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);

public function main() {
    io:println("********** COURSE OUTLINE SYSTEM **********");
    io:println("1. Log in as Student");
    io:println("2. Log in as Lecturer");
    io:println("3. Log in as HoD");
    io:println("----------------------------------------");
    string choose = io:readln("Enter Option: ");

    if (choose === "1") {
        // Ask for student password
        student();
    } else if (choose === "2") {
        // Ask for Lecturer password
        lecturer();
    } else if (choose === "3") {
        // Ask for HoD password
        HoD();
    }
}

function student() {

    io:println("********** Student Portal **********");
    io:println("1.View Course Outline ");
    io:println("2. Acknowledge");
    io:println("----------------------------------------");
    string choose = io:readln("Enter Option: ");

    if (choose === "1") {
        // 
        error? viewCourse = ViewCourse();

    } else if (choose === "2") {
        // 
        Acknowledge();
    }

}

function lecturer() {

    io:println("********** Lecture Portal **********");
    io:println("1. Input information");
    io:println("2. View Course outline he/she has generated");
    io:println("3. Generate and Sign a course outline");
    io:println("----------------------------------------");
    string choose = io:readln("Enter Option: ");

    if (choose === "1") {
        //
        inputInfo();
    } else if (choose === "2") {
        // 
        lecViewCourse();
    } else if (choose === "3") {
        lecGenCourse();
    }

}

function HoD() {

    io:println("********** HoD Portal **********");
    io:println("1. Input information about a course");
    io:println("2. View generated course outline");
    io:println("3. Generate and Sign a course outline");

    io:println("----------------------------------------");
    string choose = io:readln("Enter Option: ");

    if (choose === "1") {
        // 
        hodInputInfo();
    } else if (choose === "2") {
        // 
        ViewAllGen();
    } else if (choose === "3") {
        CourseApproval();
    }

}

//---Student Operations---
// View a course outline generated for a course he/she is taking.
function ViewCourse() returns error? {
    string message = "Hello There Guys, This is the first test";
    // Sends the message to the Kafka topic.
    check kafkaProducer->send({
                                topic: "test-kafka-topic",
                                value: message.toBytes() });

    // Flushes the sent messages.
    check kafkaProducer->'flush();
}

// Student acknowledgment of reception
function Acknowledge() {

}

//---Lecturer Operations---
// Input info about a course he/she has been assigned.
function inputInfo() {
}

// View course outline he/she has generated.
function lecViewCourse() {
}

// Generate and sign assigned course outline
function lecGenCourse() {

}

//---HoD Operations--- 
// View all generated course outlines.
function ViewAllGen() {
}

// Input info about a course, a lec and student.
function hodInputInfo() {
}

// Digitally approve a course outline
function CourseApproval() {

}

